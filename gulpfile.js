'use strict';

var gulp = require('gulp');
var livereload = require('gulp-livereload');

gulp.task('autoprefixer', function () {
    var postcss = require('gulp-postcss');
    var sourcemaps = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');

    return gulp.src('./static/css/*.css')
      .pipe(sourcemaps.init())
      .pipe(postcss([ autoprefixer() ]))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./dest'));
});

gulp.task('sass', function () {
  var sass = require('gulp-sass');

  gulp.src('./static/sass/*.sass')
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: ['node_modules/susy/sass']
    }).on('error', sass.logError))
    .pipe(gulp.dest('./static/css'))
    .pipe(livereload());
});

gulp.task('sass:watch', function () {
  gulp.watch('./static/sass/*.sass', ['sass']);
  livereload.listen();
});

gulp.task('default', function() {
  // place code for your default task here
});
