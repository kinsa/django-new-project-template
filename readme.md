A custom project template for Django.

# Installation

Create the Django project utilizing the custom template:

```sh
$ django-admin startproject --template=https://github.com/kinsa/django-newproj-template/archive/master.zip myproject
```

Install Node development modules:

```sh
$ npm install
```

Setup databases:

```sh
$ ./manage.py migrate
```

Compile Sass to CSS:

```sh
$ gulp sass
```

Smoke test:

```sh
$ ./manage.py runserver
```

# Best Practices

- [CSS Normalization](http://necolas.github.io/normalize.css/)
- [System font stack](https://bitsofco.de/the-new-system-font-stack/)
- [REM font sizing](https://snook.ca/archives/html_and_css/font-size-with-rem)
- [Typographic baseline](https://www.smashingmagazine.com/2011/03/technical-web-typography-guidelines-and-techniques/)
- Mobile-first breakpoints
- [HTML5Boilerplate HTML `meta` attributes](https://github.com/h5bp/html5-boilerplate/blob/6.0.1/dist/doc/html.md#the-html)
- [jQuery CDN for jQuery](https://github.com/h5bp/html5-boilerplate/blob/6.0.1/dist/doc/html.md#jquery-cdn-for-jquery)
- [Google Analytics Universal Tracking Code](https://github.com/h5bp/html5-boilerplate/blob/6.0.1/dist/doc/html.md#google-universal-analytics-tracking-code)
- [HTML5Boilerplate JavaScript directory design pattern](https://github.com/h5bp/html5-boilerplate/blob/6.0.1/dist/doc/js.md) with `main.js` and `plugins.js` as well as a `vendor/` directory.

# Frontend Tools

## no-js

The `html` element is affixed with the `no-js` class by default. Utilizes [Paul Irish's FUOC fix](https://www.paulirish.com/2009/avoiding-the-fouc-v3/) to replace that class with `js` if JavaScript exists in the environment. In that way styles for environments without JavaScript can be targeted by the `.no-js` selector.

If using [Modernizr](https://modernizr.com), the line: 

```html
<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
```

at the top of the `base.html` template can be removed; Modernizr will apply the `js` class itself.

## Sass Stylesheets

### Compilation

Compile Sass to CSS:

```sh
$ gulp sass
```

For development, recompile the CSS files when a Sass file is changed:

```sh
$ gulp sass:watch
```

## Autoprefixer

For CSS features in development, like `flexbox`, [Autoprefixer](https://github.com/postcss/autoprefixer) is a [PostCSS](https://github.com/postcss/postcss) plugin to add vendor prefixes to CSS rules. Called automatically after Sass gets compiled to CSS.

## LiveReload

The Gulp task for watching for Sass changes also configures LiveReload to monitor the `css` directory, use a [browser extension](http://livereload.com/extensions/) to catch changes and automatically refresh the browser:

```sh
$ gulp sass:watch
```

# Updating

`project_name/` template files copied from the latest version of [Django](https://github.com/django/django/tree/master/django/conf/project_template/project_name).

`static/sass/_normalize.scss` is the latest release from [github.com/necolas/normalize.css](https://github.com/necolas/normalize.css).

`templates/base.html` heavily influenced by the latest [HTML5Boilerplate](https://html5boilerplate.com).
